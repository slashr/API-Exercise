### Description
1. MongoDB runs using mongo:latest as a StatefulSet in order to maintain the consistency of the pod. It also uses `Persistent Volumes` to retain the DB data
2. K8S job `mongodupdater` imports into the DB data from the CSV file. Separating the import functionality from the DB reduces the possibilties of errors and enables us to import data without affecting the uptime of the DB
3. The DB is also placed in an `HorizontalPodAutoscaling` which will scale up the DB pods when the CPU Utilization goes up
4. The Node server is built using Dockerfile.node. The application and Database port numbers are configurable as `--build-args` to the docker build command.
5. `kube-manifests/node.yaml` manifest has the details of the deployment, service annd HPA similar to MongoDB.
6. I didn't spend time trying to implement the API described because it's a little out of my field of work and would need some more time and effort from me. However, I've created a simple API that writes and read timestamps from the DB
7. I've used Multistage Docker builds inside Dockerfile.node and managed to reduce image size from 336MB to 90MB

### Prerequisites
  - In order to use Persistent Volumes for MongoDB, first change the value of the Volume ID inside `kube-manifests/persistent-volume.yaml` under `spec.awsElasticBlockStore.volumeID`
  - If you do not perform this step, you will get an error about the Persistent Volume creation during deployment, but everything should work fine regardless
  - If deploying on Minikube, please edit the file `kube-manifests/node.yaml` and change the value of `node-service.spec.type` from `LoadBalancer` to `NodePort`

### Building
  - To build the docker images, run `bash buildImages.sh`
  - The two images will be tagged as `dawker/nodeapi` and `dawker/mongodbupdater` on your local machine. If you're using Minikube the images will be fetched locally but if using K8S on the cloud, the images will be pulled from my Docker Hub account.
  - To change the port the application will be available on, you can modify `port_number` inside `buildImages.sh` and `kube-manifests/node.yaml` under `node-service.spec.ports.targetPort`

### Deploying
  - To deploy the application on your K8S cluster, run `kubectl apply -f kube-manifests/`

### Using the app
1. To create a timestamp entry in the DB, go to `<loadbalancer-url>/timestamp-create`
2. To get the list of timestamps from the DB, go to `<loadbalancer-url>/timestamp`
