// connection.js
const mongoose = require("mongoose");
const Time = require("./timestamp.model"); 

const DB_PORT = process.env.DB_PORT
const dbUrl = `mongodb://mongoprod:${DB_PORT}/passengers`;
const dbConnection = () => {
 return mongoose.connect(dbUrl, {useNewUrlParser:true});
};
module.exports = dbConnection;
