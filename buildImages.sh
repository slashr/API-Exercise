#!/bin/bash

docker build -t dawker/nodeapi . -f Dockerfile.node --no-cache --build-arg port_number=8080 --build-arg db_port_number=27017 
docker build -t dawker/mongodbupdater . -f Dockerfile.mongo --no-cache
